<?php
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'dre_moduleinternals',
    'title'       => [
        'de' => '<img src="../modules/bender/dre_moduleinternals/out/img/favicon.ico" height="20px" title="" />odynova DEBUG Module Internals',
        'en' => '<img src="../modules/bender/dre_moduleinternals/out/img/favicon.ico" height="20px" title="" />odynova DEBUG Module Internals',
    ],
    'description' => [
        'en' => 'Bodynova OXID module system information and troubleshooting tools',
        'de' => 'Bodynova OXID Modulsystem Informations- und Troubleshooting Werkzeuge',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0.0',
    'author'      => 'Bodynova GmbH',
    'email'       => 'support@bodynova.de',
    'url'         => 'https://bodynova.de',
    'extend'      => [
        \OxidEsales\Eshop\Core\Module\Module::class => \Bender\dre_ModuleInternals\Core\Module::class,
    ],
    'controllers' => [
        'module_internals_metadata' => \Bender\dre_ModuleInternals\Controller\Admin\Metadata::class,
        'module_internals_state'    => \Bender\dre_ModuleInternals\Controller\Admin\State::class,
        'module_internals_utils'    => \Bender\dre_ModuleInternals\Controller\Admin\Utils::class,
    ],
    'templates'   => [
        'metadata.tpl' => 'bender/dre_moduleinternals/views/admin/tpl/metadata.tpl',
        'state.tpl'    => 'bender/dre_moduleinternals/views/admin/tpl/state.tpl',
        'utils.tpl'    => 'bender/dre_moduleinternals/views/admin/tpl/utils.tpl',
    ],
];


