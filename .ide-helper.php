<?php
namespace Bender\dre_ModuleInternals\Core;
class Module extends \OxidEsales\Eshop\Core\Module\Module{}

namespace Bender\dre_ModuleInternals\Controller\Admin;
class module_internals_metadata extends \Bender\dre_ModuleInternals\Controller\Admin\Metadata{}
class module_internals_state extends \Bender\dre_ModuleInternals\Controller\Admin\State{}
class module_internals_utils extends \Bender\dre_ModuleInternals\Controller\Admin\Utils{}
